<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    use HasFactory;
    protected $table = 'charts';
    protected $fillable = ['value'];

    public static function getTableName()
    {
        return with(new static)->getTable();
    }


}
