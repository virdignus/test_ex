<?php

namespace App\Http\Controllers;

use App\Models\Chart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    public function show()
    {
        DB::table(Chart::getTableName())->truncate();
        $dataForInsert = [];
        for ($i = 0; $i < 10; $i++) {
            $dataForInsert[] = ['value' => random_int(1, 100)];
        }
        Chart::insert($dataForInsert);
        $charts =  Chart::all();
        $result = [];

        foreach ($charts as $chart){
            $result['labels'][] =$chart->id;
            $result['values'][] = $chart->value;
        }

        return $result;
    }

    public function clear()
    {
        DB::table(Chart::getTableName())->truncate();
        return response(['message' => 'All data was cleared']);
    }
}
